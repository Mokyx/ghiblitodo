import i18n
  from 'i18next';
import { initReactI18next } from 'react-i18next';
import translationFR
  from './locales/fr/translation';

const resources = {
  fr: {
    translation: translationFR
  }
};

i18n
  .use(initReactI18next) // passes i18n down to react-i18next
  .init({
    resources,
    lng: 'fr',

    keySeparator: false, // we do not use keys in form messages.welcome

    interpolation: {
      escapeValue: false // react already safes fro  m xss
    }
  }).then(() => {});
