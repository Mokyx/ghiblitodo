import {
  ActionsTypes,
  FilmListTypes
} from '../const';
import { Map } from 'immutable';

const initialFilmListToWatch = { type: FilmListTypes.TO_WATCH, items: [], isShown: true };
const initialFilmListWatched = { type: FilmListTypes.WATCHED, items: [], isShown: true };

const initialState = {
  filmLists: Map([
    [FilmListTypes.TO_WATCH, initialFilmListToWatch],
    [FilmListTypes.WATCHED, initialFilmListWatched]
  ])
};

export function mainAppReducer (state = initialState, action) {
  switch (action.type) {
    case ActionsTypes.FILM_CLICK:
      return {
        ...state,
        filmLists:
            state.filmLists.map(filmList => ({
              ...filmList,
              items: action.payload.filmListTypeClicked === filmList.type
                ? filmList.items.filter(film => film.id !== action.payload.film.id)
                : [...filmList.items, action.payload.film]
            }))
      };
    case ActionsTypes.RECEIVE_FILMS:
      return ({
        ...state,
        filmLists:
          state.filmLists.set(
            FilmListTypes.TO_WATCH,
            { ...state.filmLists.get(FilmListTypes.TO_WATCH), items: action.payload.films }
          )
      });
    case ActionsTypes.FILTER_CLICK:
      return {
        ...state,
        filmLists:
            state.filmLists.map(filmList => ({
              ...filmList,
              isShown: action.payload.filmListType === FilmListTypes.ALL ? true : action.payload.filmListType === filmList.type
            }))
      };
    default:
      return state;
  }
}
