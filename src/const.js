export const FilmListTypes = {
  TO_WATCH: 'TO_WATCH',
  WATCHED: 'WATCHED',
  ALL: 'ALL'
};

export const ActionsTypes = {
  RECEIVE_FILMS: 'RECEIVE_FILMS',
  FILM_CLICK: 'FILM_CLICK',
  FETCH_FILMS: 'FETCH_FILMS',
  FILTER_CLICK: 'FILTER_CLICK'
};

export const GridTokens = {
  TITLE: 'title',
  FILTER: 'filter',
  LOGO: 'logo',
  DETAILS: 'details',
  TO_WATCH_HEADER: FilmListTypes.TO_WATCH + '_HEADER',
  TO_WATCH_CONTENT: FilmListTypes.TO_WATCH + '_CONTENT',
  WATCHED_HEADER: FilmListTypes.WATCHED + '_HEADER',
  WATCHED_CONTENT: FilmListTypes.WATCHED + '_CONTENT',
  _HEADER: '_HEADER',
  _CONTENT: '_CONTENT'
};

export const ResponsiveSizes = {
  XSMALL: '30em',
  SMALL: '52.5em',
  MEDIUM: '75em'
};
