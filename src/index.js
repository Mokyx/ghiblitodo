import React
  from 'react';
import ReactDOM
  from 'react-dom';
import { Provider } from 'react-redux';
import { store } from './store';
import styled
  from 'styled-components';
import './styles/app.css';
import './i18n';
import { App } from './components/App';

const RootContainer = styled.div`
  min-height:100vh;
  background-color: #282c34;
  color: aliceblue;
`;

ReactDOM.render(
  <RootContainer>
    <Provider store={store}>
      <App />
    </Provider>
  </RootContainer>,
  document.getElementById('root')
);
