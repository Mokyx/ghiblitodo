import React, { useCallback, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { filterClick } from '../action-creators/filterClick';
import { FilmListTypes, GridTokens, ResponsiveSizes } from '../const';
import { filmListsSelector } from '../selectors/selectors';
import styled from 'styled-components';
import LogoGhibli from '../res/Logo-Ghibli.png';
import { Map } from 'immutable';
import { Filters } from './Filters';
import { useTranslation } from 'react-i18next';
import { receiveFilms } from '../action-creators/receiveFilms';
import { FilmList } from './FilmList';

export const AppContainer = styled.div`
  font-family: Candara, sans-serif;
  display: grid;
  grid-template-areas:
      '${GridTokens.TITLE} ${GridTokens.FILTER} ${GridTokens.LOGO}'
      '${GridTokens.DETAILS} ${GridTokens.DETAILS} ${GridTokens.DETAILS}'
      
      ${props => props.filmLists.get(FilmListTypes.TO_WATCH).isShown
              ? '\'' + GridTokens.TO_WATCH_HEADER + ' ' + GridTokens.TO_WATCH_HEADER + ' ' + GridTokens.TO_WATCH_HEADER + '\''
              : false}
      
      ${props => props.filmLists.get(FilmListTypes.TO_WATCH).isShown
              ? '\'' + GridTokens.TO_WATCH_CONTENT + ' ' + GridTokens.TO_WATCH_CONTENT + ' ' + GridTokens.TO_WATCH_CONTENT + '\''
              : false}
      
      ${props => props.filmLists.get(FilmListTypes.WATCHED).isShown
              ? '\'' + GridTokens.WATCHED_HEADER + ' ' + GridTokens.WATCHED_HEADER + ' ' + GridTokens.WATCHED_HEADER + '\''
              : false}
      
      ${props => props.filmLists.get(FilmListTypes.WATCHED).isShown
              ? '\'' + GridTokens.WATCHED_CONTENT + ' ' + GridTokens.WATCHED_CONTENT + ' ' + GridTokens.WATCHED_CONTENT + '\''
              : false};

  @media (max-width: ${ResponsiveSizes.SMALL}) {
    grid-template-areas:
      '${GridTokens.TITLE} ${GridTokens.TITLE} ${GridTokens.LOGO}'
      '${GridTokens.FILTER} ${GridTokens.FILTER} ${GridTokens.FILTER}'
      '${GridTokens.DETAILS} ${GridTokens.DETAILS} ${GridTokens.DETAILS}'
      
      ${props => props.filmLists.get(FilmListTypes.TO_WATCH).isShown
              ? '\'' + GridTokens.TO_WATCH_HEADER + ' ' + GridTokens.TO_WATCH_HEADER + ' ' + GridTokens.TO_WATCH_HEADER + '\''
              : false}
      
      ${props => props.filmLists.get(FilmListTypes.TO_WATCH).isShown
              ? '\'' + GridTokens.TO_WATCH_CONTENT + ' ' + GridTokens.TO_WATCH_CONTENT + ' ' + GridTokens.TO_WATCH_CONTENT + '\''
              : false}
      
      ${props => props.filmLists.get(FilmListTypes.WATCHED).isShown
              ? '\'' + GridTokens.WATCHED_HEADER + ' ' + GridTokens.WATCHED_HEADER + ' ' + GridTokens.WATCHED_HEADER + '\''
              : false}
      
      ${props => props.filmLists.get(FilmListTypes.WATCHED).isShown
              ? '\'' + GridTokens.WATCHED_CONTENT + ' ' + GridTokens.WATCHED_CONTENT + ' ' + GridTokens.WATCHED_CONTENT + '\''
              : false}
  ;
  }

  grid-auto-columns: minmax(100px, 1fr);
  grid-row-gap: 30px;
  margin: 0 20px;
  padding: 20px 0;
`;

const AppTitle = styled.div`
  grid-area: ${GridTokens.TITLE};
  justify-self: left;
  font-size: 4em;
  letter-spacing: 0.75em;
  line-height: 0.9em;
  @media (max-width: ${ResponsiveSizes.MEDIUM}) {
    font-size: 2.5em;
  }
  @media (max-width: ${ResponsiveSizes.XSMALL}) {
    font-size: 1.75em;
  }
`;

const Logo = styled.img`
  grid-area: ${GridTokens.LOGO};
  justify-self: right;
  height: 100px;
  @media (max-width: ${ResponsiveSizes.MEDIUM}) {
    height: 70px;
  }
  @media (max-width: ${ResponsiveSizes.XSMALL}) {
    height: 50px;
  }
`;

const Details = styled.div`
  grid-area: ${GridTokens.DETAILS};
  justify-self: left;
`;

export function App () {
  const dispatch = useDispatch();
  const filmLists = useSelector(filmListsSelector);
  const [filtersClicked, setFiltersClicked] = useState(
    Map([
      [FilmListTypes.TO_WATCH, false],
      [FilmListTypes.WATCHED, false],
      [FilmListTypes.ALL, true]
    ])
  );

  useEffect(() => {
    fetch('https://api.themoviedb.org/3/discover/movie?api_key=f5008039fcd229372ee6bbd89d26ac20&with_companies=10342')
      .then((response) => response.json())
      .then((filmsData) => dispatch(receiveFilms(filmsData.results)));
  }, [dispatch]);

  const handleFilterClick = useCallback(
    (filmListTypeClicked) => () => {
      setFiltersClicked(
        filtersClicked
          .map(value => value ? !value : value)
          .set(filmListTypeClicked, true));
      dispatch(filterClick(filmListTypeClicked));
    },
    [dispatch, filtersClicked]
  );

  const { t } = useTranslation();

  return (
    <AppContainer filmLists={filmLists}>
      <AppTitle>{t('title1')}<br/>{t('title2')}</AppTitle>
      <Filters handleFilterClick={handleFilterClick} filtersClicked={filtersClicked}/>
      <Logo src={LogoGhibli} alt={t('altLogoGhibli')}/>
      <Details>{t('details1')}<br/>{t('details2')}<br/><br/>{t('details3')}</Details>
      {filmLists
        .valueSeq()
        .filter(filmList => filmList.isShown)
        .map(filmList => {
          return <FilmList key={filmList.type} filmList={filmList}/>;
        })}
    </AppContainer>
  );
}
