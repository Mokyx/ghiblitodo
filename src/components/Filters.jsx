import React
  from 'react';
import {
  FilmListTypes,
  GridTokens,
  ResponsiveSizes
} from '../const';
import styled
  from 'styled-components';
import PropTypes
  from 'prop-types';
import { useTranslation } from 'react-i18next';

const FiltersContainer = styled.div`
  grid-area: ${GridTokens.FILTER};
  text-align: center;
  align-self: center;
  @media(max-width: ${ResponsiveSizes.MEDIUM}){
    font-size: 0.7em;
  }
`;

const FiltersText = styled.div`
  font-size: 1.75em;
  opacity: 0.9;
  color:#7D7D7D;
  margin-bottom: 10px;
  
`;

const FilterButton = styled.button`
  background: ${props => props.clicked ? '#1F57C6' : 'none'};
  padding: 5px 10px;
  font-family: inherit;
  font-size: 1.25em;
  border: none;
  color:inherit;
  cursor:pointer;
  margin: 0 10px;
  outline-color: #1F57C6;
  &:hover{
    background: #2C4B85;
  }
  &:focus{
    background: #1F57C6;  
  }
`;

export function Filters ({ handleFilterClick, filtersClicked }) {
  const { t } = useTranslation();
  return (
    <FiltersContainer><FiltersText>{t('filterBy')}</FiltersText>
      <FilterButton clicked={filtersClicked.get(FilmListTypes.TO_WATCH)} onClick={handleFilterClick(FilmListTypes.TO_WATCH)}>{t('filterToWatch')}</FilterButton>
      <FilterButton clicked={filtersClicked.get(FilmListTypes.WATCHED)} onClick={handleFilterClick(FilmListTypes.WATCHED)}>{t('filterWatched')}</FilterButton>
      <FilterButton clicked={filtersClicked.get(FilmListTypes.ALL)} onClick={handleFilterClick(FilmListTypes.ALL)}>{t('filterAll')}</FilterButton>
    </FiltersContainer>
  );
}

Filters.propTypes = {
  handleFilterClick: PropTypes.func,
  filtersClicked: PropTypes.object
};
