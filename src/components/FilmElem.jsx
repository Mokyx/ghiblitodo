import PropTypes, { string } from 'prop-types';
import React, { useCallback, useRef, useState } from 'react';
import { useDispatch } from 'react-redux';
import { filmClick } from '../action-creators/filmClick';
import styled, { keyframes } from 'styled-components';
import { useTranslation } from 'react-i18next';

const Appear = keyframes`
  from {
    opacity: 0;
  }
  to {
    opacity: 1;
  }
`;

const ImageCover = styled.img`
  width: 200px;
  height: 300px;
  cursor: pointer;
  box-shadow: rgba(0, 0, 0, 0.64) 5px 5px 20px;
  opacity: ${props => props.clicked ? 0 : 1};
  transition: transform 0.6s, opacity 0.5s;
  animation: ${Appear} 0.5s linear;

  &:hover {
    transform: scale(1.15);
  }
`;

export function FilmElem ({ film, filmListType }) {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const [filmClicked, setFilmClicked] = useState(false);
  const clickable = useRef(true);

  const handleFilmClick = useCallback(
    () => {
      if (clickable.current) {
        clickable.current = false;
        setFilmClicked(true);
        setTimeout(() => dispatch(filmClick(film, filmListType)), 300);
      }
    },
    [dispatch, film, filmListType]
  );

  return (
    <ImageCover src={'https://image.tmdb.org/t/p/w500/' + film.poster_path} alt={t('altFilmCover')} onClick={handleFilmClick} clicked={filmClicked}/>
  );
}

FilmElem.propTypes = {
  film: PropTypes.object,
  filmListType: string
};
