import PropTypes from 'prop-types';
import React from 'react';
import { FilmElem } from './FilmElem';
import styled from 'styled-components';
import { FilmListTypes, GridTokens, ResponsiveSizes } from '../const';
import { useTranslation } from 'react-i18next';

const FilmListHeader = styled.div`
  grid-area: ${props => props.filmListType + GridTokens._HEADER};
  font-size: 1.7em;
`;

const FilmListContainer = styled.div`
  grid-area: ${props => props.filmListType + GridTokens._CONTENT};
  display: grid;
  grid-template-columns: repeat(auto-fill, minmax(200px, 1fr));
  grid-gap:30px;
  
  @media(max-width: ${ResponsiveSizes.XSMALL}){
    justify-self: center;
  }
`;

export function FilmList ({ filmList }) {
  const { t } = useTranslation();
  return (
    <>
      <FilmListHeader filmListType={filmList.type}>
        {filmList.type === FilmListTypes.TO_WATCH ? t('filmListToWatch') : t('filmListWatched')}
      </FilmListHeader>
      <FilmListContainer filmListType={filmList.type}>
        {filmList.items.map((film) => (
          <FilmElem
            key={film.id}
            film={film}
            filmListType={filmList.type}
          />
        ))}
      </FilmListContainer>
    </>
  );
}

FilmList.propTypes = {
  filmList: PropTypes.object
};
