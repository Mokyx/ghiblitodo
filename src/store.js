import {
  applyMiddleware,
  combineReducers,
  createStore
} from 'redux';
import { mainAppReducer } from './reducers/mainAppReducer';
import thunk
  from 'redux-thunk';

const rootReducer = combineReducers({
  mainApp: mainAppReducer
});

export const store = createStore(rootReducer, applyMiddleware(thunk));
