import { ActionsTypes } from '../const';

export function receiveFilms (films) {
  return {
    type: ActionsTypes.RECEIVE_FILMS,
    payload: { films }
  };
}
