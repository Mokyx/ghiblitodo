import { ActionsTypes } from '../const';

export function filterClick (filmListType) {
  return {
    type: ActionsTypes.FILTER_CLICK,
    payload: { filmListType }
  };
}
