import { ActionsTypes } from '../const';

export function filmClick (film, filmListTypeClicked) {
  return {
    type: ActionsTypes.FILM_CLICK,
    payload: { film, filmListTypeClicked }
  };
}
